import React from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  Actions,
  ActionConst,
  Scene,
  Router
} from 'react-native-router-flux';
import VectorIcon from 'react-native-vector-icons/FontAwesome';
import Intro from './components/Intro';
import SMSCodeVerification from './components/SMSCodeVerification';
import Home from './components/Home';
import BusinessList from './components/BusinessList';
import OrderList from './components/OrderList';
import Notifications from './components/Notifications';
import YourProfile from './components/YourProfile';
import RegisterBusiness from './components/RegisterBusiness';

const RouterComponent = () => {
  return (
    <Router>
        <Scene
          key='introScene'
          component={Intro}
          hideNavBar={true}
        />
        <Scene key='verification'>
          <Scene
            key='SMSCodeVerificationScene'
            component={SMSCodeVerification}
            title='Verify Your Phone'
            hideNavBar={false}
          />
        </Scene>
        <Scene key="tabBar" tabs={true} hideNavBar tabBarStyle={styles.tabBarStyle}>
          <Scene
            key="Home"
            title="Home"
            icon={() =>
              <View style={{ alignItems: 'center' }}>
                <VectorIcon name="home" size={18} />
                <Text style={{ fontSize: 10 }}>Home</Text>
              </View>
            }
            onPress={() => {
              Actions.Home({ type: ActionConst.REFRESH });
            }}
          >
             <Scene
               key="home"
               component={Home}
               hideNavBar
             />
        </Scene>
        <Scene
          key="YourBusinesses"
          title="Your Businesses"
          icon={() =>
            <View style={{ alignItems: 'center' }}>
              <VectorIcon name="briefcase" size={18} />
              <Text style={{ fontSize: 10 }}>Businesses</Text>
            </View>
            }
          onPress={() => {
            Actions.businessList({ type: ActionConst.REFRESH });
          }}
        >
          <Scene
            key="businessList"
            component={BusinessList}
            hideNavBar
          />
        </Scene>
        <Scene
          key="Orders"
          title="Orders"
          icon={() =>
            <View style={{ alignItems: 'center' }}>
              <VectorIcon name="shopping-cart" size={18} />
              <Text style={{ fontSize: 10 }}>Orders</Text>
            </View>
          }
          onPress={() => {
            Actions.orderList({ type: ActionConst.REFRESH });
          }}
        >
          <Scene
            key="orderList"
            component={OrderList}
            hideNavBar
          />
        </Scene>
        <Scene
          key="Notifications"
          title="Notifications"
          icon={() =>
            <View style={{ alignItems: 'center' }}>
              <VectorIcon name="bell-o" size={18} />
              <Text style={{ fontSize: 10 }}>Notifications</Text>
            </View>
          }
          onPress={() => {
            Actions.notifications({ type: ActionConst.REFRESH });
          }}
        >
          <Scene
            key="notifications"
            component={Notifications}
            hideNavBar
          />
        </Scene>
        <Scene
          key="YourProfile"
          title="YourProfile"
          icon={() =>
            <View style={{ alignItems: 'center' }}>
              <VectorIcon name="user-circle-o" size={18} />
              <Text style={{ fontSize: 10 }}>Your Profile</Text>
            </View>
          }
          onPress={() => {
            Actions.yourProfile({ type: ActionConst.REFRESH });
          }}
        >

          <Scene
            key="yourProfile"
            component={YourProfile}
            hideNavBar
            hideTabBar
          />
        </Scene>
      </Scene>
      <Scene
        key='registerBusiness'
        component={RegisterBusiness}
        hideNavBar={true}
      />
    </Router>
  );
};

const styles = StyleSheet.create({
        tabBarStyle: {
            height: 50,
            borderTopWidth: 1,
            borderColor: '#b7b7b7',
            backgroundColor: 'white',
            opacity: 1
        }
    });

export default RouterComponent;
