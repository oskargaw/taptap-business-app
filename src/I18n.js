import I18n from 'react-native-i18n';

// Enable fallbacks if you want `en-US` and `en-GB` to fallback to `en`
I18n.fallbacks = true;

// English language is the main language for fall back:
I18n.translations = {
  en: require('./translations/english.json')
};

let languageCode = I18n.locale.substr(0, 2);

// All other translations for the app goes to the respective language file:
switch (languageCode) {
  case 'af':
    I18n.translations.af = require('./translations/af.json');
    break;
  case 'am':
    I18n.translations.am = require('./translations/am.json');
    break;
  case 'ar':
    I18n.translations.ar = require('./translations/ar.json');
    break;
  case 'bg':
    I18n.translations.bg = require('./translations/bg.json');
    break;
  case 'ca':
    I18n.translations.ca = require('./translations/ca.json');
    break;
  case 'cs':
    I18n.translations.cs = require('./translations/cs.json');
    break;
  case 'da':
    I18n.translations.da = require('./translations/da.json');
    break;
  case 'de':
    I18n.translations.de = require('./translations/de.json');
    break;
  case 'el':
    I18n.translations.el = require('./translations/el.json');
    break;
  case 'es':
    I18n.translations.es = require('./translations/es.json');
    break;
  case 'et':
    I18n.translations.et = require('./translations/et.json');
    break;
  case 'fi':
    let addCode = I18n.locale.substr(0, 3);
    if (addCode === 'fil') {
      I18n.translations.fil = require('./translations/fil.json');
    } else {
      I18n.translations.fi = require('./translations/fi.json');
    }
    break;
  case 'fr':
    I18n.translations.fr = require('./translations/fr.json');
    break;
  case 'he':
    I18n.translations.he = require('./translations/he.json');
    break;
  case 'hi':
    I18n.translations.hi = require('./translations/hi.json');
    break;
  case 'hr':
    I18n.translations.hr = require('./translations/hr.json');
    break;
  case 'hu':
    I18n.translations.hu = require('./translations/hu.json');
    break;
  case 'in':
    I18n.translations.in = require('./translations/id.json');
    break;
  case 'id':
    I18n.translations.id = require('./translations/id.json');
    break;
  case 'it':
    I18n.translations.it = require('./translations/it.json');
    break;
  case 'ja':
    I18n.translations.ja = require('./translations/ja.json');
    break;
  case 'ko':
    I18n.translations.ko = require('./translations/ko.json');
    break;
  case 'lt':
    I18n.translations.lt = require('./translations/lt.json');
    break;
  case 'lv':
    I18n.translations.lv = require('./translations/lv.json');
    break;
  case 'ms':
    I18n.translations.ms = require('./translations/ms.json');
    break;
  case 'nb':
    I18n.translations.nb = require('./translations/nb.json');
    break;
  case 'nl':
    I18n.translations.nl = require('./translations/nl.json');
    break;
  case 'no':
    I18n.translations.no = require('./translations/no.json');
    break;
  case 'pl':
    I18n.translations.pl = require('./translations/pl.json');
    break;
  case 'pt':
    I18n.translations.pt = require('./translations/pt.json');
    break;
  case 'ro':
    I18n.translations.ro = require('./translations/ro.json');
    break;
  case 'ru':
    I18n.translations.ru = require('./translations/ru.json');
    break;
  case 'sl':
    I18n.translations.sl = require('./translations/sl.json');
    break;
  case 'sk':
    I18n.translations.sk = require('./translations/sk.json');
    break;
  case 'sr':
    I18n.translations.sr = require('./translations/sr.json');
    break;
  case 'sv':
    I18n.translations.sv = require('./translations/sv.json');
    break;
  case 'sw':
    I18n.translations.sw = require('./translations/sw.json');
    break;
  case 'th':
    I18n.translations.th = require('./translations/th.json');
    break;
  case 'tr':
    I18n.translations.tr = require('./translations/tr.json');
    break;
  case 'uk':
    I18n.translations.uk = require('./translations/uk.json');
    break;
  case 'vi':
    I18n.translations.vi = require('./translations/vi.json');
    break;
  case 'zh':
    I18n.translations.zh = require('./translations/zh.json');
    break;
  case 'zu':
    I18n.translations.zu = require('./translations/zu.json');
    break;
}

const translate = (text) => I18n.t(text);

export default translate;
