import apisauce from 'apisauce';

const create = (baseURL = 'https://rough-wave-782.getsandbox.com') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache'
    },
    timeout: 10000
  });

  const connectPhone = phone =>
        api.post('connect/phone', { phone });

  const verifyPhone = (phone, code) =>
        api.post('connect/phone/verify', { phone, code });

  const registerUser = (
    registration_token, first_name, last_name,
    email, phone, birth_date, gender,
    language, timezone, country_code
  ) =>
        api.post('connect/phone/register',
            { registration_token, first_name, last_name,
              email, phone, birth_date, gender,
              language, timezone, country_code });

  return {
    connectPhone,
    verifyPhone,
    registerUser
  };
};

export default {
  create
};
