export const FETCH_PHONE = 'FETCH_PHONE';

export const fetchPhone = (text) => {
  return {
    type: FETCH_PHONE,
    payload: text
  };
};
