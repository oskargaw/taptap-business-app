import { FETCH_PHONE } from '../actions';

const INITIAL_STATE = {
  phone: ''
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_PHONE:
      return { ...state, phone: action.payload };
    default:
      return state;
  }
}
