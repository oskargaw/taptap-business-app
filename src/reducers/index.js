import { combineReducers } from 'redux';
import VerifyReducer from './VerifyReducer';

export default combineReducers({
  phoneFetched: VerifyReducer
});
