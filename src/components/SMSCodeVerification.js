import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Akira } from 'react-native-textinput-effects';
import * as Animatable from 'react-native-animatable';
import Button from 'apsl-react-native-button';
import CountryPicker from 'react-native-country-picker-modal';
import Form from 'react-native-form';
import { fetchPhone } from '../actions';

const brandColor = '#744BAC';

class SMSCodeVerification extends Component {
  // api = {};

  constructor(props) {
    super(props);
    this.state = {
    enterCode: false,
    first_name: null,
    last_name: null,
    phone: null,
    cca2: 'PL',
    callingCode: '48'
  };
}

renderHeader() {
  if (this.state.enterCode) {
    return (
      <View style={{ paddingHorizontal: 20, paddingTop: 40, paddingBottom: 20 }}>
        <Text
          style={{
            marginBottom: 20,
            color: '#404d5b',
            fontSize: 20,
            fontWeight: 'bold',
            opacity: 0.8
          }}
        >
          Welcome back, Oskar Gawlak
        </Text>

        <Text style={styles.header} >
          What's your verification code?
        </Text>
      </View>
    );
  }

  return (
    <View style={{ paddingHorizontal: 20, paddingTop: 40, paddingBottom: 20 }}>
      <Text style={styles.header}>
        What's your phone number?
      </Text>
    </View>
  );
}

connectPhone() {
  if (this.state.enterCode) {
    return (
      Actions.tabBar({ type: 'reset' })
    );
  }

  let phone = `+${this.state.callingCode}${this.refs.form.getValues().phoneNumber}`;
  this.setState({ phone });
  this.props.fetchPhone(phone);
  this.setState({ enterCode: true });
}

  renderCountryPicker() {
    if (this.state.enterCode) {
      return (
        <View />
      );
    }

    return (
      <View style={{ paddingBottom: 12 }}>
        <CountryPicker
          ref='countryPicker'
          onChange={(country) => this.setState({
            cca2: country.cca2,
            callingCode: country.callingCode
          })}
          cca2={this.state.cca2}
          closeable
          translation='eng'
        />
      </View>
    );
  }

  renderCallingCode() {
    if (this.state.enterCode) {
      return (
        <View />
      );
    }
    if (this.state.callingCode === undefined) {
      return <Text />;
    }

    return (
      <View style={styles.callingCodeView}>
        <Text style={styles.callingCodeText}>
          {`+${this.state.callingCode}`}
        </Text>
      </View>
    );
  }

  renderFooter() {
    if (this.state.enterCode) {
      return (
        <Text style={styles.wrongNumberText}>
          Entered the wrong number or need a new code?
        </Text>
      );
    }

    return (
      <View>
        <Text style={styles.disclaimerText}>
          By tapping "Send confirmation code" above, we will send you an SMS to confirm your phone number.
          Message &amp; data rates may apply.
        </Text>
      </View>
    );
  }

  render() {
    let buttonText = this.state.enterCode ? 'Verify confirmation code' : 'Send confirmation code';

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Animatable.View animation="fadeIn" style={styles.container}>
          {this.renderHeader()}

          <Form ref="form" style={styles.form}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', paddingHorizontal: 30 }}>
              {this.renderCountryPicker()}
              {this.renderCallingCode()}
              <Akira
                ref="textInput"
                name='phoneNumber'
                type="TextInput"
                maxLength={this.state.enterCode ? 6 : 13}
                style={styles.input}
                label={this.state.enterCode ? 'Verification Code' : 'Phone Number'}
                placeholder={this.state.enterCode ? '_ _ _ _ _ _' : null}
                borderColor={'white'}
                labelStyle={styles.labelStyle}
                keyboardType="numeric"
                autoFocus
                onSubmitEditing={this.connectPhone.bind(this)}
              />
            </View>

            <Button
              style={styles.buttonStyle}
              textStyle={styles.buttonTextStyle}
              onPress={this.connectPhone.bind(this)}
            >
              {buttonText}
            </Button>

            {this.renderFooter()}
          </Form>
          <View>
            <Text>
              {this.props.phone}
            </Text>
          </View>
        </Animatable.View>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  header: {
    color: '#404d5b',
    fontSize: 20,
    fontWeight: 'bold',
    opacity: 0.8,
  },
  callingCodeView: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 30
  },
  callingCodeText: {
    marginTop: 17,
    fontSize: 20,
    color: brandColor,
    fontFamily: 'Helvetica',
    fontWeight: 'bold',
    paddingRight: 25
  },
  input: {
    width: 220,
    marginBottom: 35,
  },
  labelStyle: {
    color: '#404d5b',
    opacity: 0.6,
    fontSize: 13,
  },
  buttonStyle: {
    borderColor: '#3498db',
    backgroundColor: '#3498db',
    height: 55,
    marginHorizontal: 20
  },
  buttonTextStyle: {
    color: 'white',
    fontSize: 15
  },
  disclaimerText: {
    marginHorizontal: 25,
    marginVertical: 10,
    fontSize: 12,
    color: 'grey'
  },
  wrongNumberText: {
    margin: 10,
    fontSize: 14,
    textAlign: 'center'
  }
});

const mapStateToProps = ({ phoneFetched }) => {
  const { phone } = phoneFetched;

  return { phone };
};

export default connect(mapStateToProps, { fetchPhone })(SMSCodeVerification);
