import React, { Component } from 'react';
import {
  Container,
  Content,
  Segment,
  Header,
  Body,
  Text,
  List,
  ListItem,
  Thumbnail
} from 'native-base';
import Button from 'apsl-react-native-button';
import { Actions } from 'react-native-router-flux';

  class BusinessList extends Component {
    goToRegisterBusiness() {
      return (
        Actions.registerBusiness()
      );
    }

        render() {
            return (
            <Container>
                <Content padder>
                  <List>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Business 1</Text>
                              <Text note>Description</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Business 2</Text>
                              <Text note>Description</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Business 3</Text>
                              <Text note>Description</Text>
                          </Body>
                      </ListItem>
                  </List>

                  <Button
                    style={styles.buttonStyle}
                    textStyle={styles.buttonTextStyle}
                    onPress={this.goToRegisterBusiness.bind(this)}
                  >
                    Register New Business
                  </Button>
                </Content>
            </Container>
            );
        }
    }

    const styles = {
      buttonStyle: {
        borderColor: '#3498db',
        backgroundColor: '#3498db',
        height: 55,
        marginHorizontal: 20
      },
      buttonTextStyle: {
        color: 'white',
        fontSize: 15
      }
    };

    export default BusinessList;
