import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import AppIntro from 'react-native-app-intro';
import * as Animatable from 'react-native-animatable';
import _ from '../I18n';

class Intro extends Component {
  render() {
    return (
      <Animatable.View
        ref="introAnimation"
      >
        <AppIntro
          animation="fadeOut"
          onSkipBtnClick={() => this.refs.introAnimation.fadeOut(700)
            .then(() => Actions.verification({ type: 'reset' }))}
          onDoneBtnClick={() => Actions.verification({ type: 'reset' })}
          skipBtnLabel={_('generic.skip')}
          doneBtnLabel={_('generic.done')}
        >
          <View style={[styles.slide, { backgroundColor: '#1a931d' }]} >
              <Text style={styles.text}>
                  {_('intro.welcome')}
              </Text>
          </View>
          <View style={[styles.slide, { backgroundColor: '#fa931d' }]}>
              <Text style={styles.text}>
                  {_('intro.description.first')}
              </Text>
          </View>
            <View style={[styles.slide, { backgroundColor: '#a4b602' }]}>
              <Text style={styles.text}>
                  {_('intro.description.second')}
              </Text>
          </View>
            <View style={[styles.slide, { backgroundColor: '#fa931d' }]}>
              <Text style={styles.text}>
                  {_('intro.description.third')}
              </Text>
          </View>
          </AppIntro>
      </Animatable.View>
    );
}
}

const styles = StyleSheet.create({
  slide: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 24,
    textAlign: 'center',
    fontWeight: 'bold',
    margin: 20
  },
});

export default Intro;
