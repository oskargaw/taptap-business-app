import React, { Component } from 'react';
import {
  Container,
  Content,
  Segment,
  Header,
  Button,
  Body,
  Text,
  List,
  ListItem,
  Thumbnail
} from 'native-base';

class OrderList extends Component {
      render() {
            return (
            <Container>
              <Header hasTabs>
              <Segment style={{ paddingTop: 15 }}>
                  <Button
                    first
                    active
                  >
                    <Text>Pick-up</Text></Button>
                  <Button
                    last
                  >
                    <Text>Delivery</Text></Button>
              </Segment>
          </Header>
                <Content padder>
                  <List>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Orders</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                      <ListItem>
                          <Thumbnail square size={90} source={require('../img/order.jpg')} />
                          <Body>
                              <Text>Order</Text>
                              <Text note>Pick-up</Text>
                          </Body>
                      </ListItem>
                  </List>
                </Content>
            </Container>
            );
        }
    }

export default OrderList;
