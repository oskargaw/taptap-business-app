import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Header,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Title,
  Thumbnail
} from 'native-base';
import { Actions, ActionConst } from 'react-native-router-flux';
import VectorIcon from 'react-native-vector-icons/FontAwesome';


class YourProfile extends Component {
    goHome() {
      return (
        Actions.home({ type: ActionConst.REFRESH })
      );
    }

    render() {
        return (
            <Container>
              <Header>
                    <Left>
                        <Button
                          transparent
                          onPress={this.goHome.bind(this)}
                          >
                            <Icon name='arrow-back' />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Your Profile</Title>
                    </Body>
                    <Right>
                        <Button
                          transparent
                          onPress={this.goHome.bind(this)}
                          >
                            <Icon name='md-checkmark' />
                        </Button>
                    </Right>
                </Header>
                    <Content>
                      <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginTop: 30,
                        marginBottom: 40
                      }}>
                        <Thumbnail
                          circular source={require('../img/man.jpeg')}
                          style={{ marginHorizontal: 22 }}
                         />
                         <Text style={{ fontSize: 20 }}>Oskar Gawlak</Text>
                       </View>
                        <Form>
                            <Item stackedLabel>
                                <Label>First Name</Label>
                                <Input />
                            </Item>
                            <Item stackedLabel>
                                <Label>Last Name</Label>
                                <Input />
                            </Item><Item stackedLabel>
                                <Label>Email Address</Label>
                                <Input />
                            </Item><Item stackedLabel>
                                <Label>Gender</Label>
                                <Input />
                            </Item><Item stackedLabel>
                                <Label>Birth Date</Label>
                                <Input />
                            </Item><Item stackedLabel>
                                <Label>Language</Label>
                                <Input />
                            </Item>
                            <Item stackedLabel last>
                                <Label>Timezone</Label>
                                <Input />
                            </Item>
                        </Form>
                    </Content>
                </Container>
        );
    }
}

const styles = {
  buttonStyle: {
    borderColor: '#3498db',
    backgroundColor: '#3498db',
    height: 55,
    marginHorizontal: 20
  },
  buttonTextStyle: {
    color: 'white',
    fontSize: 15
  }
}

export default YourProfile;
